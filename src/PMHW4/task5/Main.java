package PMHW4.task5;

import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        System.out.println(makeStringsUpperCase(List.of("abc", "def", "qqq")));
    }
    public static String makeStringsUpperCase(List<String> listOfStrings) {
        return listOfStrings.stream()
                .map(String::toUpperCase)
                .collect(Collectors.joining(", "));
    }
}
