package PMHW4.task3;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        System.out.println(countNotEmptyStrings(List.of("abc", "", "", "def", "qqq")));
    }
    public static long countNotEmptyStrings(List<String> listOfStrings)  {
        return listOfStrings.stream()
                .filter(str -> !str.isEmpty()).count();
    }
}
