package PMHW4.task2;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        System.out.println(getMultiplyResult(List.of(1, 2, 3, 4, 5)));
    }

    public static int getMultiplyResult(List<Integer> listOfIntegers)  {
        return listOfIntegers.stream()
                .reduce(1, (x , y) -> x * y);
    }
}
