package PMHW4.task4;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        System.out.println(reverseSortOfNumbers(List.of(1, 2, 3, 4, 5)));
    }
    public static List<Integer> reverseSortOfNumbers(List<Integer> listOfNumbers)   {
        return listOfNumbers.stream()
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());
    }
}
