package PMHW4.additional1;

public class Main {

    public static void main(String[] args) {
        System.out.println(levenshteinDistance("cat", "ca"));
    }

    public static boolean levenshteinDistance(String first, String second) {
        int[][] array = new int[first.length() + 1][second.length() + 1];
        for (int i = 0; i <= first.length(); i++) {
            array[i][0] = i;
        }
        for (int j = 0; j <= second.length(); j++) {
            array[0][j] = j;
        }
        for (int i = 1; i <= first.length(); i++) {
            for (int j = 1; j <= second.length(); j++) {
                int cost = first.charAt(i - 1) == second.charAt(j - 1) ? 0 : 1;
                array[i][j] = Math.min(Math.min(array[i - 1][j] + 1, array[i][j - 1] + 1), array[i - 1][j - 1] + cost);
            }
        }
        return array[first.length()][second.length()] <= 1;
    }
}
