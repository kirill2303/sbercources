package PMHW4.task1;

import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) {

        int sum = IntStream.rangeClosed(1, 100).filter(x -> x % 2 == 0).sum();
        System.out.println(sum);
    }
}
