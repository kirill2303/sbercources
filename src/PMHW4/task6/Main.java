package PMHW4.task6;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Set<Set<Integer>> setOfSets = new HashSet<>();
        setOfSets.add(new HashSet<>(Arrays.asList(1, 2, 3)));
        setOfSets.add(new HashSet<>(Arrays.asList(3, 4, 5)));
        setOfSets.add(new HashSet<>(Arrays.asList(5, 6, 7, 8, 9)));
        System.out.println(convertToSet(setOfSets));
    }
    public static Set<Integer> convertToSet(Set<Set<Integer>> setOfSetsOfIntegers)  {
        return setOfSetsOfIntegers.stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }
}
