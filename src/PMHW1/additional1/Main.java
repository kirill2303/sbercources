package PMHW1.additional1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] intArray = new int[n];

        for (int i = 0; i < intArray.length; i++) {
            intArray[i] = scanner.nextInt();
        }
        /*
        Наверное более элегантным решением было бы отсортировать массив и вывести 2 последних элемента, например:
        Arrays.sort(intArray);
        System.out.println(intArray[intArray.length - 1] + " " + intArray[intArray.length - 2]);
        но так будет проигрывать по потребляемым ресурсам, поэтому:
         */
        int maxNumber = Integer.MIN_VALUE;
        int secondMaxNumber = Integer.MIN_VALUE;

        for (int i = 0; i < intArray.length; i++) {
            if (intArray[i] > maxNumber) {
                secondMaxNumber = maxNumber;
                maxNumber = intArray[i];
            } else if (intArray[i] > secondMaxNumber) {
                secondMaxNumber = intArray[i];
            }
        }
        System.out.println(maxNumber + " " + secondMaxNumber);
    }
}
