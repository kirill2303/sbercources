package PMHW1.task4;

public class MyPrimeNumberException extends RuntimeException{

    public MyPrimeNumberException()   {
        super("You can't create prime number");
    }

    public MyPrimeNumberException(String msg)   {
        super("You can't create prime number " + msg);
    }
}
