package PMHW1.task4;

public class Main {
    public static void main(String[] args) {
        MyEvenNumber evenNumber = new MyEvenNumber(2);
        System.out.println(evenNumber.getN());

        MyEvenNumber notEvenNumber = new MyEvenNumber(3);
        System.out.println(notEvenNumber.getN());
    }
}
