package PMHW1.task4;

public class MyEvenNumber {
    private int n;

    public MyEvenNumber(int n){
        if (n % 2 != 0) {
            throw new MyPrimeNumberException();
        }
        this.n = n;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }
}
