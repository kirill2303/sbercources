package PMHW1.additional2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        int p = scanner.nextInt();

        System.out.println(binarySearch(array, p));
    }

    public static int binarySearch(int[] array, int p) {

        int low = 0;
        int high = array.length - 1;

        while (high >= low) {
            int mid = low + (high - low) / 2;
            if (array[mid] == p) {
                return mid;
            } else if (array[mid] < p) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }
        return -1;
    }
}
