package PMHW1.task3;

import java.io.*;

public class Main {

    private static final String DIRECTORY = "ENTER YOUR PATH TO THE DIRECTORY HERE";
    private static final String INPUT_FILE_NAME = "input.txt";
    private static final String OUTPUT_FILE_NAME = "output.txt";

    public static void main(String[] args) {
        toUpperCase();
    }

    public static void toUpperCase() {
        try (BufferedReader br = new BufferedReader(new FileReader(DIRECTORY + INPUT_FILE_NAME));
        BufferedWriter bw = new BufferedWriter(new FileWriter(DIRECTORY + OUTPUT_FILE_NAME)))
        {
            while (br.ready())  {
                bw.write(br.readLine().toUpperCase());
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
