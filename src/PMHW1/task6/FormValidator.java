package PMHW1.task6;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class FormValidator {

    public static void main(String[] args) {
        checkName("Вячеслав");
        checkBirthdate("11.11.1999");
        checkGender("Female");
        checkHeight("123");
    }

    public static void checkName(String str) throws RuntimeException {
        if (str.charAt(0) != Character.toUpperCase(str.charAt(0))) {
            throw new RuntimeException("Первая буква должна быть заглавная");
        }
        if (str.length() < 2 || str.length() > 20) {
            throw new RuntimeException("Длина имени должна быть от 2 до 20 символов");
        }
    }

    public static void checkBirthdate(String str) throws RuntimeException {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate birthDate = LocalDate.parse(str, formatter);
        LocalDate lowLimitDate = LocalDate.of(1900, 1, 1);

        if (birthDate.isBefore(lowLimitDate) || birthDate.isAfter(LocalDate.now())) {
            throw new RuntimeException("Дата рождения должна быть не раньше 01.01.1900 и не позже текущей даты");
        }
    }

    public static void checkGender(String str) throws RuntimeException {
        if (!(Gender.Male.toString().equals(str) || Gender.Female.toString().equals(str))) {
            throw new RuntimeException("Пол должен корректно матчится в enum Gender, хранящий Male и Female значения");
        }
    }

    public static void checkHeight(String str) throws RuntimeException {

        try {
            double height = Double.parseDouble(str);
            if (height < 0) {
                throw new RuntimeException("Рост должен быть положительным числом");
            }
        } catch (NumberFormatException e)   {
            throw new RuntimeException("Некорректно введен рост");
        }
    }
}
