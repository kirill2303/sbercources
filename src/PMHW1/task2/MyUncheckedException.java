package PMHW1.task2;

public class MyUncheckedException extends RuntimeException{

    public MyUncheckedException(String msg) {
        super(msg);
    }
}
