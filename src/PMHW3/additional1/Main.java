package PMHW3.additional1;

import java.util.EmptyStackException;
import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        System.out.println("Checking case 1, (()()()) is correct bracket sequence?: "
                + isCorrectBracketSequence("(()()())"));
        System.out.println("Checking case 2, )( is correct bracket sequence?: "
                + isCorrectBracketSequence(")("));
        System.out.println("Checking case 3, (() is correct bracket sequence?: "
                + isCorrectBracketSequence("(()"));
        System.out.println("Checking case 4, ((())) is correct bracket sequence?: "
                + isCorrectBracketSequence("((()))"));
    }

    public static boolean isCorrectBracketSequence(String s)    {

        if (s.isEmpty())    {
            return true;
        }
        if (s.charAt(0) == ')') {
            return false;
        }

        Stack<Character> stack = new Stack<>();

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == ')') {
                try {
                    if (stack.pop() == '(') {
                        continue;
                    } else {
                        return false;
                    }
                } catch (EmptyStackException e) {
                    return false;
                }
            }
            stack.push(c);
        }
        return stack.isEmpty();
    }
}
