package PMHW3.task4;

import PMHW3.task4.structure.A;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Class<?>> interfaces = new ArrayList<>();
        getAllInterfaces(interfaces, A.class);
        interfaces.forEach(System.out::println);
    }

    public static void getAllInterfaces(List<Class<?>> interfaces, Class<?> clazz) {
        while (clazz != Object.class) {
            getInterfacesFromCurrentClazz(interfaces, clazz);
            clazz = clazz.getSuperclass();
        }
    }

    public static void getInterfacesFromCurrentClazz(List<Class<?>> interfaces, Class<?> clazz) {
        List<Class<?>> currentClazzInterfaces = Arrays.asList(clazz.getInterfaces());
        if (currentClazzInterfaces.size() > 0) {
            interfaces.addAll(currentClazzInterfaces);
            for (int i = 0; i < currentClazzInterfaces.size(); i++) {
                if (currentClazzInterfaces.get(i).getInterfaces().length > 0) {
                    getInterfacesFromCurrentClazz(interfaces, currentClazzInterfaces.get(i));
                }
            }
        }
    }
}
