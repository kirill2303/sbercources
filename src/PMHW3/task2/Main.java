package PMHW3.task2;

import PMHW3.task1.IsLike;

public class Main {

    public static void main(String[] args) {
        System.out.println(hasIsLikeAnnotation(A.class));
        System.out.println(hasIsLikeAnnotation(B.class));
        System.out.println(hasIsLikeAnnotation(C.class));
    }

    public static boolean hasIsLikeAnnotation(Class<?> clazz)   {
        if (!clazz.isAnnotationPresent(IsLike.class))   {
            return false;
        }
        IsLike isLike = clazz.getAnnotation(IsLike.class);
        return isLike.isLike();
    }
}
