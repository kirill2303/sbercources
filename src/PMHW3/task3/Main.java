package PMHW3.task3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {

    public static void main(String[] args) {
        callMethodPrint();
    }

    public static void callMethodPrint() {

        APrinter aPrinter = new APrinter();
        try {
            Method method = aPrinter.getClass().getMethod("print", int.class);
            method.invoke(aPrinter, 1);
        } catch (IllegalAccessException e) {
            System.out.println("The method access modifiers forbid calling it");
        } catch (InvocationTargetException e) {
            System.out.println("The method throw an exception");
        } catch (NoSuchMethodException e) {
            System.out.println("No such method");
        }
    }
}
