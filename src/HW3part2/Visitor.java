package HW3part2;

public class Visitor {

    private String name;
    private String takenBook;
    private int libraryId = -1;

    public Visitor(String name) {
        this.name = name;
        this.takenBook = null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTakenBook() {
        return takenBook;
    }

    public void setTakenBook(String takenBook) {
        this.takenBook = takenBook;
    }

    public void setLibraryId(int libraryId) {
        this.libraryId = libraryId;
    }

    public int getLibraryId() {
        return libraryId;
    }

    @Override
    public String toString() {
        return "Посетитель '" + name +
                "' книга на руках '" + takenBook +
                "' id в библиотеке: " + libraryId;
    }
}
