package HW3part2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Library {

    private static final List<Book> listOfBooks = new ArrayList<>();
    private static final List<Visitor> listOfVisitors = new ArrayList<>();
    private static int currentVisitorId;

    public static void addBook(Book book) {
        boolean exists = false;
        for (Book b : listOfBooks) {
            if (b.getName().equals(book.getName())) {
                System.out.println("Книга с названием '" + b.getName() + "' уже есть в библиотеке" +
                        (b.isAvailable() ? ", в настоящий момент она доступна." : ", в настоящий момент она недоступна"));
                exists = true;
            }
        }
        if (!exists) {
            listOfBooks.add(book);
        }
    }

    public static void removeBook(String bookName) {
        if (listOfBooks.removeIf(book -> book.getName().equals(bookName) && book.isAvailable())) {
            System.out.println("Книга с названием '" + bookName + "' удалена из библиотеки");
        } else {
            System.out.println("Книги с названием '" + bookName + "' на текущий момент нет в библиотеке");
        }
    }

    public static Book getBook(String bookName) {
        return listOfBooks.stream().filter(b -> b.getName().equals(bookName)).findAny().orElse(null);
    }

    public static List<Book> getBooksByAuthor(String AuthorName) {
        return listOfBooks.stream().filter(b -> b.getAuthor().equals(AuthorName)).collect(Collectors.toList());
    }

    public static void addVisitor(Visitor visitor) {
        listOfVisitors.add(visitor);
        visitor.setLibraryId(currentVisitorId++);
    }

    public static void assignBook(Visitor visitor, String bookName) {
        if (listOfVisitors.stream().noneMatch(v -> v.getName().equals(visitor.getName()))) {
            addVisitor(visitor);
            System.out.println("Посетитель '" + visitor.getName() +
                    "' добавлен в список библиотеки, его id равен " + visitor.getLibraryId());
        }
        for (Visitor v : listOfVisitors) {
            if (v.getName().equals(visitor.getName())) {
                if (v.getTakenBook() == null) {
                    boolean exists = false;
                    for (Book book : listOfBooks) {
                        if (book.getName().equals(bookName) && book.isAvailable()) {
                            v.setTakenBook(bookName);
                            book.setAvailable(false);
                            System.out.println("Книга '" + bookName + "' взята посетителем '" +
                                    v.getName() + "' с id равным " + v.getLibraryId());
                            exists = true;
                            break;
                        }
                    }
                    if (!exists)    {
                        System.out.println("Книги с названием '" + bookName + "' на текущий момент нет в библиотеке");
                    }
                } else {
                    System.out.println("У посетителя '" + v.getName() + "' уже есть книга '" + v.getTakenBook() + "'");
                }
            }
        }
    }

    public static void returnBook(Visitor visitor) {
        Book rateBook = null;
        boolean exists = false;
        for (Visitor v : listOfVisitors) {
            if (v.getName().equals(visitor.getName()) && v.getTakenBook() != null) {
                System.out.println("Посетитель '" + v.getName() + "' вернул книгу '" +
                        v.getTakenBook() + "'");
                for (Book book : listOfBooks) {
                    if (book.getName().equals(v.getTakenBook())) {
                        book.setAvailable(true);
                        rateBook = book;
                    }
                }
                v.setTakenBook(null);
                exists = true;
            }
        }
        if (!exists) {
            System.out.println("У посетителя '" + visitor.getName() + "' нет книг на руках");
        }
        if (rateBook != null)   {
            Scanner scanner = new Scanner(System.in);
            int rate;
            do {
                System.out.print("Оцените книгу " + rateBook.getName() + " по шкале от 1 до 10: ");
                rate = scanner.nextInt();
            }
            while (rate < 1 || rate > 10);
            rateBook.getBookRating().add(rate);
        }
    }

    public static double getBookRating(String bookName)    {
        return listOfBooks.stream().filter(b -> b.getName().equals(bookName)).findAny().get()
                .getBookRating().stream().mapToInt(Integer::intValue).average().orElse(0.0);
    }



    public static void main(String[] args) {

        fillBooksList();

        System.out.println("Задание №1:");
        addBook(new Book("Книга №1", "Автор1")); //проверяем занесение повторной книги
        addBook(new Book("Книга №100", "Автор1")); //проверяем занесение новой книги
        listOfBooks.forEach(System.out::println);

        System.out.println("Задание №2: ");
        removeBook("Книга №3"); //проверяем удаление существующей книги
        removeBook("Книга №999"); //проверяем удаление несуществующей книги
        listOfBooks.forEach(System.out::println);

        System.out.println("Задание №3: ");
        System.out.println(getBook("Книга №10")); //получаем существующую книгу по названию
        System.out.println(getBook("Книга №101")); //получаем несуществующую книгу по названию

        System.out.println("Задание №4: ");
        getBooksByAuthor("Автор1").forEach(System.out::println); //получаем список по имени автора

        System.out.println("Задание №5: ");
        addVisitor(new Visitor("Посетитель1")); //добавляем нового посетителя
        assignBook(new Visitor("Посетитель1"), "Книга №1"); //назначаем книгу существующему посетителю
        assignBook(new Visitor("Посетитель2"), "Книга №2"); //назначаем книгу новому посетителю
        assignBook(new Visitor("Посетитель2"), "Книга №3"); //назначаем книгу посетителю у которого уже есть книга
        listOfBooks.forEach(System.out::println);
        listOfVisitors.forEach(System.out::println);

        System.out.println("Задание №6: ");
        returnBook(new Visitor("Посетитель2")); //посетитель с книгой возвращает ее
        returnBook(new Visitor("Посетитель3")); //посетитель без книги возвращает книгу
        listOfBooks.forEach(System.out::println);
        listOfVisitors.forEach(System.out::println);

        System.out.println("Задание №8: ");
        assignBook(new Visitor("Посетитель3"), "Книга №2"); //задаем еще раз книгу посетителю
        returnBook(new Visitor("Посетитель3")); //возвращаем книгу чтобы собрать оценки
        System.out.println("Средняя оценки книги: " + getBookRating("Книга №2")); //выводим оценку книги
    }

    public static void fillBooksList() {

        addBook(new Book("Книга №1", "Автор1"));
        addBook(new Book("Книга №2", "Автор2"));
        addBook(new Book("Книга №3", "Автор3"));
        addBook(new Book("Книга №4", "Автор2"));
        addBook(new Book("Книга №5", "Автор2"));

    }
}
