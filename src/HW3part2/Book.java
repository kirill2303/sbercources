package HW3part2;

import java.util.ArrayList;
import java.util.List;

public class Book {

    private String name;
    private String author;
    private boolean available = true;
    private List<Integer> bookRating = new ArrayList<>();

    public Book(String name, String author) {
        this.name = name;
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public List<Integer> getBookRating() {
        return bookRating;
    }

    @Override
    public String toString() {
        return "Книга '" + name +
                "' Автор '" + author + (available ?
                "' доступна" : " недоступна");
    }
}
