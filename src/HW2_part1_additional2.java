import java.util.Arrays;
import java.util.Scanner;

/*
2.	(3 балла) Решить задачу 7 основного дз за линейное время.
Про понятие линейного времени можно почитать здесь:
https://ru.wikipedia.org/wiki/%D0%92%D1%80%D0%B5%D0%BC%D0%B5%D0%BD%D0%BD%D0%B0%D1%8F_%D1%81%D0%BB%D0%BE%D0%B6%D0%BD%D0%BE%D1%81%D1%82%D1%8C_%D0%B0%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC%D0%B0
 */
public class HW2_part1_additional2 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        sort(array);

        for (int i : array) {
            System.out.print(i + " ");
        }
    }

    public static void sort(int[] array)    {

        int left = 0;
        int right = array.length - 1;
        int[] temp = new int[array.length];

        for (int i = temp.length - 1; i >= 0  ; i--) {
            if (Math.abs(array[left]) > Math.abs(array[right])){
                temp[i] = array[left] * array[left];
                left++;
            } else {
                temp[i] = array[right] * array[right];
                right--;
            }
        }

        for (int i = 0; i < array.length; i++)  {
            array[i] = temp[i];
        }
    }
}
