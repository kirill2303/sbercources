import java.util.Random;
import java.util.Scanner;

/*
1.	(2 балла) Создать программу генерирующую пароль.
На вход подается число N — длина желаемого пароля. Необходимо проверить, что N >= 8,
иначе вывести на экран "Пароль с N количеством символов небезопасен" (подставить вместо N число)
и предложить пользователю еще раз ввести число N.

Если N >= 8 то сгенерировать пароль, удовлетворяющий условиям ниже и вывести его на экран.
В пароле должны быть:
●	заглавные латинские символы
●	строчные латинские символы
●	числа
●	специальные знаки(_*-)

 */

public class HW2_part1_additional1 {

    private static final String LOWER_CASE = "abcdefghijklmnopqrstuvwxyz";
    private static final String UPPER_CASE = LOWER_CASE.toUpperCase();
    private static final String NUMBER = "0123456789";
    private static final String SPECIAL = "_*-";
    private static final String FULL_STRING = LOWER_CASE + UPPER_CASE + NUMBER + SPECIAL;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите длину желаемого пароля, не менее 8 символов: ");

        int n;
        while ((n = scanner.nextInt()) < 8) {
            System.out.println("Пароль с " + n + " количеством символов небезопасен");
            System.out.print("Введите длину желаемого пароля, не менее 8 символов: ");
        }

        passwordCreator(n);

    }

    public static void passwordCreator(int passwordLength)   {
        StringBuilder stringBuilder = new StringBuilder(passwordLength);
        int i = 0;
        while (i < passwordLength) {
            while (i < passwordLength - 3) {
                stringBuilder.append(randomGenerator(FULL_STRING));
                i++;
            }
            if (!stringBuilder.toString().matches(".*[" + LOWER_CASE + "].*")) {
                stringBuilder.append(randomGenerator(LOWER_CASE));
                i++;
            } else if (!stringBuilder.toString().matches(".*[" + UPPER_CASE + "].*")) {
                stringBuilder.append(randomGenerator(UPPER_CASE));
                i++;
            } else if (!stringBuilder.toString().matches(".*[" + NUMBER + "].*")) {
                stringBuilder.append(randomGenerator(NUMBER));
                i++;
            } else if (!stringBuilder.toString().matches(".*[" + SPECIAL + "].*")) {
                stringBuilder.append(randomGenerator(SPECIAL));
                i++;
            } else {
                stringBuilder.append(randomGenerator(FULL_STRING));
                i++;
            }
        }
        System.out.println(stringBuilder);
    }

    public static char randomGenerator(String charSeq) {
        return charSeq.charAt(new Random().nextInt(charSeq.length()));
    }
}
