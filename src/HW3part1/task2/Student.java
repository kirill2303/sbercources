package HW3part1.task2;

import HW3part1.task3.StudentService;

import java.util.Arrays;

/*
2.	Необходимо реализовать класс Student.
У класса должны быть следующие приватные поля:
●	String name — имя студента
●	String surname — фамилия студента
●	int[] grades — последние 10 оценок студента. Их может быть меньше, но не может быть больше 10.
И следующие публичные методы:
●	геттер/сеттер для name
●	геттер/сеттер для surname
●	геттер/сеттер для grades
●	метод, добавляющий новую оценку в grades. Самая первая оценка должна быть удалена, новая должна сохраниться в конце массива (т.е. массив должен сдвинуться на 1 влево).
●	метод, возвращающий средний балл студента (рассчитывается как среднее арифметическое от всех оценок в массиве grades)

 */
public class Student {

    private String name;
    private String surname;
    private int[] grades = new int[10];

    public static void main(String[] args) {

        Student student1 = new Student();
        student1.setGrades(new int[]{5, 4, 4, 4, 4, 4, 4, 4, 4, 0});
        student1.setSurname("Якунин");
        student1.addGrade(3);

        Student student2 = new Student();
        student2.setGrades(new int[]{5, 5, 4, 4, 4, 4, 5, 4, 4, 5});
        student2.setSurname("Абдуллин");
        student2.addGrade(5);

        Student student3 = new Student();
        student3.setGrades(new int[]{5, 2, 4, 4, 4, 4, 5, 4, 0, 0});
        student3.setSurname("Иванов");
        student3.addGrade(2);

        Student student4 = new Student();
        student4.setGrades(new int[]{5, 5, 4, 4, 4, 4, 5, 4, 4, 5});
        student4.setSurname("Романов");
        student4.addGrade(4);

        Student[] students = {student1, student2, student3, student4};

        System.out.println("Студент с самым высоким средним баллом: " + StudentService.bestStudent(students).getSurname());

        StudentService.sortBySurname(students);
        System.out.println("Отсортированный по фамилии список студентов: ");
        for (Student s : students)  {
            System.out.println(s.getSurname());
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    public void addGrade(int grade) {
        int temp = 0;
        for (int i = 0; i < grades.length; i++) {
            if (grades[i] == 0) {
                grades[i] = grade;
                temp++;
                break;
            }
        }
        if (temp == 0) {
            for (int i = 0; i < grades.length - 1; i++) {
                grades[i] = grades[i + 1];
            }
            grades[grades.length - 1] = grade;
        }
    }

    public double averageGrade() {
        return Arrays.stream(grades).average().getAsDouble();
    }
}
