package HW3part1.task8;

public class Atm {

    private final double rublesToDollarsExchangeRate;
    private final double dollarsToRublesExchangeRate;
    private static int counter;

    public Atm(double rublesToDollarsExchangeRate, double dollarsToRublesExchangeRate) {
        if (rublesToDollarsExchangeRate < 0 || dollarsToRublesExchangeRate < 0) {
            throw new IllegalArgumentException("Значение курсов валют должно быть положительным");
        }
        this.rublesToDollarsExchangeRate = rublesToDollarsExchangeRate;
        this.dollarsToRublesExchangeRate = dollarsToRublesExchangeRate;
        counter++;
    }

    public void dollarsToRubles(double dollars) {
        System.out.printf("Перевод долларов в рубли: %.2f$ --> %.2f₽\n",
                dollars, dollars * dollarsToRublesExchangeRate);
    }

    public void rublesToDollars(double rubles) {
        System.out.printf("Перевод рублей в доллары: %.2f₽ --> %.2f$\n",
                rubles, rubles * rublesToDollarsExchangeRate);
    }

    public static void showInstanceCounter() {
        System.out.println(counter);
    }
}
