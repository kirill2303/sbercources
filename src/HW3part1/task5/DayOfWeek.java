package HW3part1.task5;

public class DayOfWeek {
    private final byte dayOfWeek;
    private final String name;

    public DayOfWeek(byte dayOfWeek, String name) {
        this.dayOfWeek = dayOfWeek;
        this.name = name;
    }

    public byte getDayOfWeek() {
        return dayOfWeek;
    }

    public String getName() {
        return name;
    }
}
