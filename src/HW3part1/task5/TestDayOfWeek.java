package HW3part1.task5;

import java.util.Arrays;

public class TestDayOfWeek {
    public static void main(String[] args) {
        DayOfWeek[] week = new DayOfWeek[7];

        week[0] = new DayOfWeek((byte) 1, "Monday");
        week[1] = new DayOfWeek((byte) 2, "Tuesday");
        week[2] = new DayOfWeek((byte) 3, "Wednesday");
        week[3] = new DayOfWeek((byte) 4, "Thursday");
        week[4] = new DayOfWeek((byte) 5, "Friday");
        week[5] = new DayOfWeek((byte) 6, "Saturday");
        week[6] = new DayOfWeek((byte) 7, "Sunday");

        Arrays.stream(week).forEach(day -> System.out.println(day.getDayOfWeek() + " " + day.getName()));
    }
}
