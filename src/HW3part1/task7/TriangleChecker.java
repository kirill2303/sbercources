package HW3part1.task7;

import java.util.Random;

public class TriangleChecker {

    public static boolean correctTriangle(double a, double b, double c) {
        return ((a + b > c) && (b + c > a) && (c + a > b));
    }

    public static void main(String[] args) {

        System.out.println("Возможно составить треугольник из сторон a=2.0 b=1.0 c=3.0 ? :" +
                correctTriangle(2, 1, 3));
        System.out.println("Возможно составить треугольник из сторон a=2.0 b=2.0 c=3.0 ? :" +
                correctTriangle(2, 2, 3));

        someRandomTests();
    }

    public static void someRandomTests()    {
        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            double a = random.nextDouble(10);
            double b = random.nextDouble(10);
            double c = random.nextDouble(10);

            System.out.println("Возможно составить треугольник из сторон a=" + a +
                    " b=" + b + " c=" + c + " ? :" + correctTriangle(a, b, c));
        }
    }
}
