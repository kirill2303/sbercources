package HW3part1.task6;

public class AmazingString {

    char[] amazingString;

    public AmazingString(char[] string) {
        this.amazingString = string;
    }

    public AmazingString(String string) {
        this.amazingString = new char[string.length()];
        for (int i = 0; i < amazingString.length; i++) {
            amazingString[i] = string.charAt(i);
        }
    }

    public char getCharByIndex(int i) {
        return amazingString[i];
    }

    public int getStringLength() {
        return amazingString.length;
    }

    public void printString() {
        System.out.println(amazingString);
    }

    public boolean checkStringContainingChar(char[] charArray) {
        boolean result = false;
        for (int i = 0; i < amazingString.length; i++) {
            if (amazingString[i] == charArray[0]) {
                result = true;
                for (int j = 0; j < charArray.length; j++) {
                    if (i + charArray.length > amazingString.length)   {
                        return false;
                    }
                    if (amazingString[i + j] != charArray[j]) {
                        result = false;
                        break;
                    }
                }
                if (result) {
                    return result;
                }
            }
        }
        return result;
    }


    public boolean checkStringContainingString(String string) {
        char[] tempString = new char[string.length()];
        for (int i = 0; i < tempString.length; i++) {
            tempString[i] = string.charAt(i);
        }
        return checkStringContainingChar(tempString);
    }

    public void trimStringFromBeginning() {
        if (amazingString[0] == ' ') {
            int count = 1;
            for (int j = 1; j < amazingString.length; j++) {
                if (amazingString[j] == ' ') {
                    count++;
                }
                else break;
            }
            char[] tempAray = new char[amazingString.length - count];
            for (int i = 0; i < tempAray.length; i++) {
                tempAray[i] = amazingString[i + count];
            }
            amazingString = tempAray;
        }
    }

    public void reverseString() {
        char temp = ' ';
        for (int i = 0; i < amazingString.length / 2; i++) {
            temp = amazingString[i];
            amazingString[i] = amazingString[amazingString.length - i - 1];
            amazingString[amazingString.length - i - 1] = temp;
        }
    }
}
