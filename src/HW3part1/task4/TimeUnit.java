package HW3part1.task4;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class TimeUnit {
    private LocalTime localTime;


    public TimeUnit(int hours, int minute, int seconds) {
        this.localTime = LocalTime.of(hours, minute, seconds);
    }

    public TimeUnit(int hours, int minute) {
        this.localTime = LocalTime.of(hours, minute, 0);
    }

    public TimeUnit(int hours) {
        this.localTime = LocalTime.of(hours, 0, 0);
    }

    public void setLocalTime(LocalTime localTime) {
        this.localTime = localTime;
    }
    public LocalTime getLocalTime() {
        return localTime;
    }

    public void showTime24HFormat()   {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        System.out.println(getLocalTime().format(dateTimeFormatter));
    }
    public void showTime12HFormat()   {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("hh:mm:ss a");
        System.out.println(getLocalTime().format(dateTimeFormatter));
    }

    public void addTime(int hours, int minutes, int seconds)    {
        setLocalTime(localTime.plusHours(hours).plusMinutes(minutes).plusSeconds(seconds));
    }

    public static void main(String[] args) {

        TimeUnit timeUnit = new TimeUnit(10, 25);

        timeUnit.showTime24HFormat();
        timeUnit.showTime12HFormat();

        timeUnit.addTime(2, 10, 50);

        timeUnit.showTime24HFormat();
        timeUnit.showTime12HFormat();

    }
}
