package HW3part1.task1;

/*
1.	Необходимо реализовать класс Cat.
У класса должны быть реализованы следующие приватные методы:
●	sleep() — выводит на экран “Sleep”
●	meow() — выводит на экран “Meow”
●	eat() — выводит на экран “Eat”
И публичный метод:
status() — вызывает один из приватных методов случайным образом.
 */

import java.util.Random;

class Cat   {

    public static void main(String[] args) {
        Cat cat = new Cat();
        cat.status();
    }

    private void sleep()    {
        System.out.println("Sleep");
    }

    private void meow()    {
        System.out.println("Meow");
    }

    private void eat()    {
        System.out.println("Eat");
    }

    public void status()    {
        int random = new Random().nextInt(3);
        switch (random) {
            case 1: meow();
            break;
            case 2: eat();
            break;
            default: sleep();
        }
    }
}