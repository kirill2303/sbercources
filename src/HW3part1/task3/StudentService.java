package HW3part1.task3;
/*
3.	Необходимо реализовать класс StudentService.
У класса должны быть реализованы следующие публичные методы:
●	bestStudent() — принимает массив студентов (класс Student из предыдущего задания), возвращает лучшего студента (т.е. который имеет самый высокий средний балл). Если таких несколько — вывести любого.
●	sortBySurname() — принимает массив студентов (класс Student из предыдущего задания) и сортирует его по фамилии.

 */
import HW3part1.task2.Student;

import java.util.Arrays;
import java.util.Comparator;

public class StudentService {

    static double maxAverage = 0;
    static int currentBestStudent = 0;

    public static Student bestStudent(Student[] students)    {
        for (int i = 0; i < students.length; i++) {
            if (students[i].averageGrade() > maxAverage)    {
                maxAverage = students[i].averageGrade();
                currentBestStudent = i;
            }
        }
        return students[currentBestStudent];
    }

    public static Student[] sortBySurname(Student[] students)    {
        Arrays.sort(students, new Comparator<Student>() {
            @Override
            public int compare(Student t1, Student t2) {
                return t1.getSurname().compareTo(t2.getSurname());
            }
        });
        return students;
    }
}
