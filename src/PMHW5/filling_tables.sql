--insert into flowers table
INSERT INTO flowers (name, price)
VALUES ('Rose', 100);
INSERT INTO flowers (name, price)
VALUES ('Lilly', 50);
INSERT INTO flowers (name, price)
VALUES ('Chamomile', 25);

--insert into customers table
INSERT INTO customers (name, phone_number)
VALUES ('Michael', '1-234-456789');
INSERT INTO customers (name, phone_number)
VALUES ('Johny', '9-876-654321');
INSERT INTO customers (name, phone_number)
VALUES ('Liza', '0-123-789456');

--insert into orders table
INSERT INTO orders (customer_id, flower_id, flowers_count, date_of_order)
VALUES (1, 1, 100, now());
INSERT INTO orders (customer_id, flower_id, flowers_count, date_of_order)
VALUES (1, 2, 50, now());
INSERT INTO orders (customer_id, flower_id, flowers_count, date_of_order)
VALUES (1, 3, 1000, now());
INSERT INTO orders (customer_id, flower_id, flowers_count, date_of_order)
VALUES (2, 1, 10, now());
INSERT INTO orders (customer_id, flower_id, flowers_count, date_of_order)
VALUES (2, 2, 500, now());
INSERT INTO orders (customer_id, flower_id, flowers_count, date_of_order)
VALUES (3, 3, 1, now());
INSERT INTO orders (customer_id, flower_id, flowers_count, date_of_order)
VALUES (1, 1, 100, now() - INTERVAL '2 month');