--1.	По идентификатору заказа получить данные заказа и данные клиента,  создавшего этот заказ
SELECT * FROM orders o
LEFT JOIN customers c
ON c.id = o.customer_id
WHERE o.id = 6;

--2.	Получить данные всех заказов одного клиента по идентификатору клиента за последний месяц
SELECT * FROM orders
WHERE customer_id = 1
AND date_of_order >= now() - INTERVAL '1 month';

--3.	Найти заказ с максимальным количеством купленных цветов, вывести их название и количество
SELECT o.id AS "Order ID", f.name AS "Flower name", o.flowers_count AS "Quantity" FROM orders o
JOIN flowers f on o.flower_id = f.id
WHERE o.flowers_count = (SELECT MAX(flowers_count) FROM orders);

--4.	Вывести общую выручку (сумму золотых монет по всем заказам) за все время
SELECT SUM(o.flowers_count * f.price) AS "Total gain, gold coins" FROM orders o
JOIN flowers f on o.flower_id = f.id;