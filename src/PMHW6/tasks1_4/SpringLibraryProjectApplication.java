package PMHW6.tasks1_4;

import com.bagrov.springlibraryproject.dbexample.dao.BookDAOBean;
import com.bagrov.springlibraryproject.dbexample.dao.UserDAOBean;
import com.bagrov.springlibraryproject.dbexample.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.Date;
import java.util.Calendar;
import java.util.Scanner;

@SpringBootApplication
public class SpringLibraryProjectApplication implements CommandLineRunner {

    BookDAOBean bookDAOBean;
    UserDAOBean userDAOBean;

    @Autowired
    public SpringLibraryProjectApplication(BookDAOBean bookDAOBean, UserDAOBean userDAOBean) {
        this.bookDAOBean = bookDAOBean;
        this.userDAOBean = userDAOBean;
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringLibraryProjectApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        User user1 = createUser1();
        User user2 = createUser2();

        userDAOBean.addUser(user1);
        userDAOBean.addUser(user2);

        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите телефонный номер в формате (x-xxx-xxx-xx-xx) или email в формате (xxx@xxx.xxx): ");
        String input = scanner.nextLine();
        userDAOBean.getBooksInfo(input);
    }

    public User createUser1()   {
        User user1 = new User();
        user1.setSurname("Петров");
        user1.setName("Петр");
        Calendar calendar = Calendar.getInstance();
        calendar.set(1960, Calendar.APRIL, 17);
        user1.setDateOfBirth(new Date(calendar.getTimeInMillis()));
        user1.setPhoneNumber("8-888-888-88-88");
        user1.setEmail("abc@abc.xyz");
        user1.addBook("firstBook");
        user1.addBook("secondBook");
        user1.addBook("thirdBook");
        user1.addBook("fourthBook");
        user1.addBook("fifthBook");

        return user1;
    }

    public User createUser2()   {
        User user2 = new User();
        user2.setSurname("Иванов");
        user2.setName("Иван");
        Calendar calendar = Calendar.getInstance();
        calendar.set(1970, Calendar.MARCH, 28);
        user2.setDateOfBirth(new Date(calendar.getTimeInMillis()));
        user2.setPhoneNumber("7-777-777-77-77");
        user2.setEmail("xyz@xyz.abc");
        user2.addBook("secondBook");
        user2.addBook("fourthBook");
        user2.addBook("fifthBook");

        return user2;
    }
}
