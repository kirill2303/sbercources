insert into books(title, author, date_added)
VALUES ('firstBook', '1 author', now());
insert into books(title, author, date_added)
VALUES ('secondBook', '2 author', now());
insert into books(title, author, date_added)
VALUES ('thirdBook', '3 author', now());
insert into books(title, author, date_added)
VALUES ('fourthBook', '4 author', now());
insert into books(title, author, date_added)
VALUES ('fifthBook', '5 author', now());