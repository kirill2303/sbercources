package PMHW6.tasks1_4.model;


import lombok.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private int id;
    private String surname;
    private String name;
    private Date dateOfBirth;
    private String phoneNumber;
    private String email;
    private List<String> listOfBooks;

    public void addBook(String bookName)    {
        if (listOfBooks == null)  {
            listOfBooks = new ArrayList<>();
        }
        listOfBooks.add(bookName);
    }
    public String getBooks()    {
        return String.join(", ", listOfBooks);
    }
}
