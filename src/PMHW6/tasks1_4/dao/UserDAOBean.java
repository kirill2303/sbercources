package PMHW6.tasks1_4.dao;

import com.bagrov.springlibraryproject.dbexample.model.Book;
import com.bagrov.springlibraryproject.dbexample.model.User;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.*;

@Component
@Scope("prototype")
public class UserDAOBean {
    private final Connection connection;
    private final String ADD_USER = "insert into users(surname, name, date_of_birth, phone_number, email, list_of_books)\n" +
            "VALUES (?, ?, ?, ?, ?, ?)";
    private final String GET_LIST_OF_BOOKS_BY_PHONE_NUMBER = "select list_of_books from users where phone_number = ?";
    private final String GET_LIST_OF_BOOKS_BY_EMAIL = "select list_of_books from users where email = ?";
    private final String GET_BOOKS_INFO = "select * from books where title = ?";

    public UserDAOBean(Connection connection) {
        this.connection = connection;
    }

    public void addUser(User user) throws SQLException {
        PreparedStatement addQuery = connection.prepareStatement(ADD_USER);
        addQuery.setString(1, user.getSurname());
        addQuery.setString(2, user.getName());
        addQuery.setDate(3, (Date) user.getDateOfBirth());
        addQuery.setString(4, user.getPhoneNumber());
        addQuery.setString(5, user.getEmail());
        addQuery.setString(6, user.getBooks());
        addQuery.executeUpdate();
    }

    public void getBooksInfo(String phoneNumberOrEmail) throws SQLException {
        PreparedStatement ps1;
        if (phoneNumberOrEmail.matches("^\\d-\\d{3}-\\d{3}-\\d{2}-\\d{2}$")) {
            ps1 = connection.prepareStatement(GET_LIST_OF_BOOKS_BY_PHONE_NUMBER);
        } else if (phoneNumberOrEmail.matches("^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$")) {
            ps1 = connection.prepareStatement(GET_LIST_OF_BOOKS_BY_EMAIL);
        } else throw new SQLException("Введено некорректное значение");
        ps1.setString(1, phoneNumberOrEmail);
        ResultSet rs = ps1.executeQuery();
        String listOfBooks = "";
        if (rs.next()) {
            listOfBooks = rs.getString("list_of_books");
        } else System.out.println("Информация не найдена");
        String[] books = listOfBooks.split(", ");
        for (int i = 0; i < books.length; i++) {
            PreparedStatement ps2 = connection.prepareStatement(GET_BOOKS_INFO);
            ps2.setString(1, books[i]);
            ResultSet resultSet = ps2.executeQuery();
            Book book = new Book();

            while (resultSet.next()) {
                book.setId(resultSet.getInt("id"));
                book.setAuthor(resultSet.getString("author"));
                book.setTitle(resultSet.getString("title"));
                book.setDateAdded(resultSet.getDate("date_added"));
                System.out.println(book);
            }
        }
    }
}
