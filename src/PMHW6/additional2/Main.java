package PMHW6.additional2;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число: ");
        int n = scanner.nextInt();
        System.out.print(isPrimeNUmber(n) ? n + " - простое число" : n + " - не является простым числом");
    }

    public static boolean isPrimeNUmber(int number) {
        for (int i = number - 1; i > 1; i--) {
            if (number % i == 0)  {
                return false;
            }
        }
        return true;
    }
}