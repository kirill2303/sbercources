package PMHW6.additional1;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число: ");
        int n = scanner.nextInt();
        System.out.print(isArmstrongNumber(n) ? n + " - число Армстронга" : n + " - не является числом Армстронга");
    }

    public static boolean isArmstrongNumber(int number) {

        int armstrongNumber = number;
        String stringValueOfNumber = String.valueOf(number);
        int numberLength = stringValueOfNumber.length();

        int result = 0;

        while (number != 0) {
            int temp = number % 10;
            result += Math.pow(temp, numberLength);
            number /= 10;
        }

        return result == armstrongNumber;
    }
}
