package PMHW2.task2;

import java.util.Arrays;
import java.util.Objects;

public class Main {
    public static void main(String[] args) {

        System.out.println(isAnagram("привет", "притев"));

    }

    public static boolean isAnagram(String s, String t) {
        if (Objects.isNull(s) || Objects.isNull(t) || s.length() != t.length()) {
            return false;
        }
        char[] sArray = s.toCharArray();
        char[] tArray = t.toCharArray();

        Arrays.sort(sArray);
        Arrays.sort(tArray);

        return Arrays.equals(sArray, tArray);
    }

}
