package PMHW2.task3;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {

        Set<Integer> set1 = new TreeSet<>(Arrays.asList(1, 2, 3));
        Set<Integer> set2 = new TreeSet<>(Arrays.asList(0, 1, 2, 4));

        System.out.println(intersection(set1, set2));
        System.out.println(union(set1, set2));
        System.out.println(relativeComplement(set1, set2));
    }

    public static <T> Set<T> intersection(Set<T> set1, Set<T> set2)    {
        Set<T> resultSet = new TreeSet<>(set1);
        resultSet.retainAll(set2);
        return resultSet;
    }

    public static <T> Set<T> union(Set<T> set1, Set<T> set2)    {
        Set<T> resultSet = new TreeSet<>(set1);
        resultSet.addAll(set2);
        return resultSet;
    }

    public static <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2)    {
        Set<T> resultSet = new TreeSet<>();
        for (T element : set1)  {
            if (!set2.contains(element)) {
                resultSet.add(element);
            }
        }
        return resultSet;
    }
}
