package PMHW2.additional;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        String[] words = {"the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is", "day"};
        System.out.println(getMaxRepeatedWords(words, 4));

    }

    public static String getMaxRepeatedWords(String[] input, int k) {

        NavigableMap<String, Integer> map = new TreeMap<>();
        for (String s : input) {
            map.compute(s, (String key, Integer oldValue) -> {
                if (oldValue != null) {
                    return oldValue + 1;
                } else {
                    return 1;
                }
            });
        }

        NavigableMap<String, Integer> descendingMap = map.descendingMap();
        List<Map.Entry<String, Integer>> list = new ArrayList<>(descendingMap.entrySet());

        list.sort((Map.Entry<String, Integer> firstEntry, Map.Entry<String, Integer> secondEntry) -> {
            return secondEntry.getValue().compareTo(firstEntry.getValue());
        });

        int currentIndex = 0;

        String[] result = new String[k];
        for (Map.Entry<String, Integer> entry : list) {
            result[currentIndex] = entry.getKey();
            currentIndex++;
            if (currentIndex == k) {
                break;
            }
        }
        return Arrays.toString(result);
    }
}
