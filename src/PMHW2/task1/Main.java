package PMHW2.task1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();

        list.add(1);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(5);
        list.add(5);

        System.out.println(getUniqueElements(list));
    }

    public static <T> Set<T> getUniqueElements(List<T> list)    {
        return new HashSet<>(list);
    }

}
