package PMHW2.task4;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        List<Document> documentList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Document document = new Document();
            document.id = i;
            document.name = "Имя документа №" + i;
            documentList.add(document);
        }

        Map<Integer, Document> organizedDocuments = organizeDocuments(documentList);

        System.out.println(organizedDocuments);
    }

    public static Map<Integer, Document> organizeDocuments(List<Document> documents)   {
        Map<Integer, Document> map = new HashMap<>();
        for (Document doc : documents)  {
            map.put(doc.id, doc);
        }
        return map;
    }
}
