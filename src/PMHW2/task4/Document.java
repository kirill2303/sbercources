package PMHW2.task4;

public class Document {

    public int id;
    public String name;
    public int pageCount;

    @Override
    public String toString() {
        return "(Document " +
                "id=" + id +
                ", name='" + name + '\'' + ")";
    }
}
