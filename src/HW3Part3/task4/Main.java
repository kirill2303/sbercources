package HW3Part3.task4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int quantity = scanner.nextInt();
        scanner.nextLine();

        for (int i = 0; i < quantity; i++) {
            Participant.getListOfParticipants()
                    .add(scanner.nextLine());
        }

        for (int i = 0; i < quantity; i++) {
            Dog.getListOfDogs()
                    .add(scanner.nextLine());
        }

        double[][] marks = new double[quantity][3];
        for (int i = 0; i < marks.length; i++) {
            for (int j = 0; j < marks[i].length; j++) {
                marks[i][j] = scanner.nextDouble();
            }
        }

        double maxPoints = 0;
        int[] places = new int[3];
        for (int i = 0; i < marks.length; i++) {
            double currentPoints = 0;
            for (int j = 0; j < marks[i].length; j++) {
                currentPoints += marks[i][j];
            }
            currentPoints /= 3;
            if ( currentPoints > maxPoints) {
                maxPoints = currentPoints;
                places[2] = places[1];
                places[1] = places[0];
                places[0] = i;
            }
        }
        double scale = Math.pow(10, 1);

        for (int i = 0; i < 3; i++) {
            double average = 0;
            for (int j = 0; j < 3; j++) {
                average += marks[places[i]][j];
            }
            average = Math.floor(average / 3  * scale) / scale;
            System.out.println(Participant.getListOfParticipants().get(places[i]) +
                    ": " + Dog.getListOfDogs().get(places[i]) +
                    ", " + average);
        }
    }
}