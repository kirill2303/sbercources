package HW3Part3.task2;

public class BestCarpenterEver {

    public static void main(String[] args) {
        Chair chair = new Chair();
        System.out.println(canFix(chair));

        String string = "String";
        System.out.println(canFix(string));

        Table table = new Table();
        System.out.println(canFix(table));
    }

    public static boolean canFix(Object o) {
        return o instanceof Chair;
    }

}
