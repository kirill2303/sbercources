package HW3Part3.task3;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Task3 {

    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int m = scanner.nextInt();

        int[][] array = new int[m][n];

        List<String> list = new ArrayList<>();

        for (int i = 0; i < array.length; i++) {
            StringBuilder row = new StringBuilder();
            for (int j = 0; j < array[i].length; j++) {
                row.append(i + j).append(" ");
            }
            list.add(row.toString().trim());
        }
        list.forEach(System.out::println);
    }
}
