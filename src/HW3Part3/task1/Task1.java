package HW3Part3.task1;

public class Task1 {
    public static void main(String[] args) {
        Bat bat = new Bat();
        bat.eat();
        bat.sleep();
        bat.flySpeed();
        bat.wayOfBirth();

        Dolphin dolphin = new Dolphin();
        dolphin.eat();
        dolphin.sleep();
        dolphin.swimSpeed();
        dolphin.wayOfBirth();

        GoldFish goldFish = new GoldFish();
        goldFish.eat();
        goldFish.sleep();
        goldFish.swimSpeed();
        goldFish.wayOfBirth();

        Eagle eagle = new Eagle();
        eagle.eat();
        eagle.sleep();
        eagle.flySpeed();
        eagle.wayOfBirth();
    }
}
